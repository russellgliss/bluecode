<style type="text/css" media="screen">@import "themes/jqt/theme.css";</style>
<script type="text/javascript" charset="utf-8">
var jQT = new $.jQTouch({
	icon: 'jqtouch.png',
	addGlossToIcon: false,
	startupScreen: 'jqt_startup.png',
	statusBar: 'black',
	preloadImages: [
								'themes/jqt/img/back_button.png',
								'themes/jqt/img/back_button_clicked.png',
								'themes/jqt/img/grayButton.png',
								'themes/jqt/img/whiteButton.png',
								'themes/jqt/img/loading.gif'
	]
});
</script>