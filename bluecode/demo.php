<html>
    <head>
        <meta charset="UTF-8" />
        <title>jQTouch &beta;</title>
        <style type="text/css" media="screen">@import "../../jqtouch/jqtouch.min.css";</style>
        <style type="text/css" media="screen">@import "../../themes/apple/theme.min.css";</style>
        <script src="../../jqtouch/jquery.1.3.2.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../jqtouch/jqtouch.min.js" type="application/x-javascript" charset="utf-8"></script>
        <script type="text/javascript" charset="utf-8">
            var jQT = new $.jQTouch({
                icon: 'jqtouch.png',
                addGlossToIcon: false,
                startupScreen: 'jqt_startup.png',
                statusBar: 'black';
            });
            </script>
            <style type="text/css" media="screen">
                body.fullscreen #home .info {
                    display: none;
                }
                #about {
                    padding: 100px 10px 40px;
                    text-shadow: rgba(255, 255, 255, 0.3) 0px -1px 0;
                    font-size: 13px;
                    text-align: center;
                    background: #161618;
                }
                #about p {
                    margin-bottom: 8px;
                }
                #about a {
                    color: #fff;
                    font-weight: bold;
                    text-decoration: none;
                }
            </style>
          </head>
          <body>
         

        <div id="home" class="current">
            <div class="toolbar">
                <h1>jQTouch</h1>
                <a class="button slideup" id="infoButton" href="#about">About</a>
            </div>
            <ul class="rounded">
                <li class="arrow"><a href="#ui">User Interface</a> <small class="counter">4</small></li>
                <li class="arrow"><a href="#animations">Animations</a> <small class="counter">8</small></li>
                <li class="arrow"><a href="#ajax">AJAX</a> <small class="counter">3</small></li>
                <li class="arrow"><a href="#callbacks">Callback Events</a> <small class="counter">3</small></li>
                <li class="arrow"><a href="#extensions">Extensions</a> <small class="counter">4</small></li>
                <li class="arrow"><a href="#demos">Demos</a> <small class="counter">2</small></li>
            </ul>
            <h2>External Links</h2>
            <ul class="rounded">
                <li class="forward"><a href="http://www.jqtouch.com/" target="_blank">Homepage</a></li>
                <li class="forward"><a href="http://www.twitter.com/jqtouch" target="_blank">Twitter</a></li>
                <li class="forward"><a href="http://code.google.com/p/jqtouch/w/list" target="_blank">Google Code</a></li>
            </ul>
            <ul class="individual">
                <li><a href="&#109;&#097;&#105;&#108;&#116;&#111;:&#100;&#107;&#064;&#109;&#111;&#114;&#102;&#117;&#110;&#107;&#046;&#099;&#111;&#109;" target="_blank">Email</a></li>
                <li><a href="http://tinyurl.com/support-jqt" target="_blank">Donate</a></li>
            </ul>
            <div class="info">
                <p>Add this page to your home screen to view the custom icon, startup screen, and full screen mode.</p>
            </div>
        </div>
        
       </body>
</html>