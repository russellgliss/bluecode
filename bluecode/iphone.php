<style type="text/css" media="screen">@import "themes/apple/theme.css";</style>
<script type="text/javascript" charset="utf-8">
var jQT = new $.jQTouch({
	icon: 'jqtouch.png',
	addGlossToIcon: false,
	startupScreen: 'jqt_startup.png',
	statusBar: 'black',
	preloadImages: [
								'themes/apple/img/back_button.png',
								'themes/apple/img/back_button_clicked.png',
								'themes/apple/img/grayButton.png',
								'themes/apple/img/whiteButton.png',
								'themes/apple/img/loading.gif'
	]
});
</script>