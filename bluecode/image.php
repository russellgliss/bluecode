<?php

	header( "Content-Type: image/jpeg" );

	$img = @imagecreatefromjpeg( $_REQUEST["image"] );
	list( $width, $height ) = getimagesize( $_REQUEST["image"] );
	
	if ( isset( $_REQUEST["width"] ) ) {
		$w = $_REQUEST["width"];
		$h = ( $w / $width ) * $height;
	} elseif ( isset( $_REQUEST["height"] ) ) {
		$h = $_REQUEST["height"];
		$w = ( $h / $height ) * $width;		
	} else {
		$w = 300;
		$h = ( $w / 300 ) * $height;
			}
	$thumb = imagecreatetruecolor( $w, $h );
	
	imagecopyresized($thumb, $img, 0, 0, 0, 0, $w, $h, $width, $height);	
	
	imagejpeg( $thumb );
	imagedestroy( $thumb );
	imagedestroy( $img );

?>