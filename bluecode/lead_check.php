#!/usr/local/bin/php.cli
<?php

	require_once( "db.php" );

	$mbox= imap_open("{localhost:995/pop3/ssl/novalidate-cert}", "bluecode", "B1u3c0d3");
	
	//header("Content-Type:image/gif");
	$numMessages = imap_num_msg( $mbox );
	if ( $numMessages > 0 ) {
		echo "Found " . $numMessages . " message(s)<br>";
		
		//echo "<table>";		
		//echo "<tr><th>From</th><th>Subject</th><th>Date</th><th>Attachment</th><th>Data</th></tr>";
		
		for ($msgno = 1; $msgno <= $numMessages; $msgno++ ) {
			$lead = array();
			echo "<br><hr><br>Message : " . $msgno . "<br>";
			$header = imap_header( $mbox, $msgno );
			
			$lead["email"] = strtolower( $header->from[0]->mailbox . "@" . $header->from[0]->host );
			$lead["date_submitted"] = date("Y-m-d H:i:s", $header->udate );
						
			$struct = imap_fetchstructure($mbox,$msgno);
			$contentParts = count($struct->parts);
			 
			if ($contentParts >= 2) {
				for ($i=2;$i<=$contentParts;$i++) {
					$att[$i-2] = imap_bodystruct($mbox,$msgno,$i);
				}
				//print_r( $struct );
				for ($k=0;$k<sizeof($att);$k++) {
					if ( $att[$k]->ifparameters == 1 ) {
						if ($att[$k]->parameters[0]->value == "us-ascii" || $att[$k]->parameters[0]->value    == "US-ASCII") {
							if ($att[$k]->parameters[1]->value != "") {
								$selectBoxDisplay[$k] = $att[$k]->parameters[1]->value;
							}
						} elseif ($att[$k]->parameters[0]->value != "iso-8859-1" &&    $att[$k]->parameters[0]->value != "ISO-8859-1") {
							$selectBoxDisplay[$k] = $att[$k]->parameters[0]->value;
						}
					} else { // Blackberry						
						if ( $att[$k]->ifdparameters == 1 ) {
							$t = $att[$k]->dparameters[0]->value;
							$t = substr( $t, 10 );
							$selectBoxDisplay[$k] = base64_decode( substr( $t, 0, strlen( $t ) - 2) );
						}
					}
				}
			}
			print_r( $selectBoxDisplay );
			
			if (sizeof($selectBoxDisplay) > 0) {
				for ($j=0;$j<sizeof($selectBoxDisplay);$j++) {
					$filename = strtolower( $selectBoxDisplay[$j] );
					$ext = substr( $filename, -4 ); 
					if ( ( $ext == ".jpg" ) || ( $ext == "jpeg" ) ) {
						$f = fopen( "temp/" . $lead["email"] . "_" . $filename, "w" );
						$data = imap_fetchbody( $mbox, $msgno, $j+2 );
						fwrite( $f, imap_base64( $data ) );
						fclose( $f );
						$md5 = md5_file( "temp/" . $lead["email"] . "_" . $filename );
						rename("temp/" . $lead["email"] . "_" . $filename, "images/" . $md5 . ".jpg" );
						$lead["image"][] = $md5;
						$filename = "images/" . $md5 . ".jpg";
												
						$exif = getGPS( $filename );						
						if ( $exif != null ) {
							//print_r( $exif );
							$lead["lat"] = $exif[0];
							$lead["log"] = $exif[1];
						} else {
							$lead["lat"] = "0";
							$lead["log"] = "0";
						}												
					}
				}
						
			
				//print_r( $lead );
				//echo "<br><br>";
				
				$query = "insert into `lead` ( `email`, `date_submitted`, `latitude`, `longitude`, `image` ) values " .
					"( '" . $lead["email"] . "', '" . $lead["date_submitted"] . "', " . $lead["lat"] . ", " . $lead["log"] . ", '" . $lead["image"][0] . "')";
				echo $query;
				if ( $result = mysql_query( $query ) ) {
					$query = "select count(*) cnt from `user` where `email` = '" . $lead["email"] . "'";
					//echo $query;
					$result = mysql_query( $query );
					$row = mysql_fetch_array( $result );
					if ( $row["cnt"] == 0 ) {
						echo "<br>Creating user : " .  $lead["email"];
						$query = "insert into `user` ( `email`, `password` ) values ('" . $lead["email"] . "', '" . md5("chep1234") . "')";
						echo $query;
						$result = mysql_query( $query );					
					}
					imap_delete($mbox, $msgno);
				} else {
					echo "Error";
				}
			}			
		}
		//echo "<table>";
		
	} else {
		//echo "No new emails";
	}

	imap_close( $mbox, CL_EXPUNGE );
	
	function processStructure( $part ) {
		if ( isset( $part->disposition ) ) { // check for disposition
			if ( strtoupper ( $part->disposition ) == "ATTACHMENT" ) {
				if ( strtoupper( $part->subtype ) == "JPEG" ) {
					echo $part->description;
					//print_r( $part );
				}					
			}
		}
	}	
	
	function getGPS($image) {
		$exif = exif_read_data($image, 0, true);
		if ($exif){
			$lat = $exif['GPS']['GPSLatitude'];
			$log = $exif['GPS']['GPSLongitude'];
			if (!$lat || !$log) return null;
			// latitude values //
			$lat_degrees = divide($lat[0]);
			$lat_minutes = divide($lat[1]);
			$lat_seconds = divide($lat[2]);
			$lat_hemi = $exif['GPS']['GPSLatitudeRef'];
	
			// longitude values //
			$log_degrees = divide($log[0]);
			$log_minutes = divide($log[1]);
			$log_seconds = divide($log[2]);
			$log_hemi = $exif['GPS']['GPSLongitudeRef'];
	
			$lat_decimal = toDecimal($lat_degrees, $lat_minutes, $lat_seconds, $lat_hemi);
			$log_decimal = toDecimal($log_degrees, $log_minutes, $log_seconds, $log_hemi);
	
			return array($lat_decimal, $log_decimal);
		}	else{
			return null;
		}
	}	
	
	function toDecimal($deg, $min, $sec, $hemi)	{
		$d = $deg + $min/60 + $sec/3600;
		return ($hemi=='S' || $hemi=='W') ? $d*=-1 : $d;
	}
	
	function divide($a) {
		// evaluate the string fraction and return a float //
		$e = explode('/', $a);
		// prevent division by zero //
		if (!$e[0] || !$e[1]) {
			return 0;
		}	else{
			return $e[0] / $e[1];
		}
	}	
	
	function rotateImage( ) {
		$exif = exif_read_data($filename);
		$ort = $exif['IFD0']['Orientation'];
    switch($ort) {
        case 1: // nothing
        break;

        case 2: // horizontal flip
            $image->flipImage($public,1);
        break;
                                
        case 3: // 180 rotate left
            $image->rotateImage($public,180);
        break;
                    
        case 4: // vertical flip
            $image->flipImage($public,2);
        break;
                
        case 5: // vertical flip + 90 rotate right
            $image->flipImage($public, 2);
                $image->rotateImage($public, -90);
        break;
                
        case 6: // 90 rotate right
            $image->rotateImage($public, -90);
        break;
                
        case 7: // horizontal flip + 90 rotate right
            $image->flipImage($public,1);    
            $image->rotateImage($public, -90);
        break;
                
        case 8:    // 90 rotate left
            $image->rotateImage($public, 90);
        break;
    }


    //$image->rotateImage() is inspired by example of http://php.net/manual/en/function.imagerotate.php
    //$image->flipImage() is inspired by http://php.net/manual/en/function.imagecopy.php#42803 (thank you)	}
    
	}
	


/*	
	
	[subtype] => JPEG
	[subtype] => GIF
	[bytes] => 6450
		
	[description] => image.jpg
		
	[disposition] => ATTACHMENT
	
	
	
     function getdecodevalue($message,$parttype,$partencoding) {
          switch($parttype) {
               case 0: //text, handle encoding
                    switch($partencoding) {
                         case 3:
                              $message=base64_decode($message);
                              break;
                         case 4:
                              $message=quoted_printable_decode($message);
                              break;
                    }
               case 1: //multipart
                    $message = imap_8bit($message);
                    break;
               case 2: //message
                    $message = imap_binary($message);
                    break;
               case 3: //application
               case 4: //audio
                    $message = imap_qprint($message);
                    break;
               case 5: //image
                    $message=imap_base64($message);
                    break;
               case 6: //video
               case 7: //other
          }
          return $message;
     }	
	
			$data = imap_fetchbody( $mbox, )
			$f = fopen( $part->description, "w" );
			fwrite( $f, imap_base64( $part ) );
			fclose( $f );			
	
	
	
	
	
0 multipart/mixed
   1 multipart/alternative
       1.1 text/plain
       1.2 text/html
   2 message/rfc822
       2 multipart/mixed
           2.1 multipart/alternative
               2.1.1 text/plain
               2.1.2 text/html
           2.2 message/rfc822
               2.2 multipart/alternative
                   2.2.1 text/plain
                   2.2.2 text/html	
	
	
*/
	
?>