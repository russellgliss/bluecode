<?php 
	
	session_start();

	function htmlbegin() {
		?>
		<html>
			<head>
				<title>Bluecode</title>
				<style type="text/css" media="screen">@import "jqtouch/jqtouch.min.css";</style>
				<!--  <style type="text/css" media="screen">@import "themes/apple/theme.min.css";</style> -->
				<script src="jqtouch/jquery.1.3.2.min.js" type="text/javascript" charset="utf-8"></script>
				<script src="jqtouch/jqtouch.min.js" type="application/x-javascript" charset="utf-8"></script>
				<script src="extensions/jqt.location.js" type="application/x-javascript" charset="utf-8"></script>
				<?php 
				if ( preg_match( "/iPhone/", $_SERVER["HTTP_USER_AGENT"] ) ) {
					require_once("iphone.php");
				} elseif ( preg_match( "/Android/", $_SERVER["HTTP_USER_AGENT"] ) ) {
					require_once("android.php");
				} else {
					require_once("iphone.php");
				}
				?>
				<script type="text/javascript" charset="utf-8">
					$(function(){
						function setDisplay(text) {
							$('.info').empty().append(text)
						}
                
						// We pass "updateLocation" a callback function,
						// to run once we have the coordinates.
						// We also set it to a variable, so we can know
						// right away if it's working or not
						var lookup = jQT.updateLocation(function(coords){
							if (coords) {
								setDisplay('Latitude: ' + coords.latitude + '<br />Longitude: ' + coords.longitude);
							} else {
								setDisplay('Device not capable of geo-location.');
							}
						});

						if (lookup) {
							setDisplay('Looking up location&hellip;');
						}
					});					
				</script>
				<style type="text/css" media="screen">
          body.fullscreen #home .info {
            display: none;
          }
          #about {
            padding: 100px 10px 40px;
            text-shadow: rgba(255, 255, 255, 0.3) 0px -1px 0;
            font-size: 13px;
            text-align: center;
            background: #161618;
          }
          #about p {
            margin-bottom: 8px;
          }
          #about a {
            color: #fff;
            font-weight: bold;
            text-decoration: none;
          }
         </style>  		
			</head>
			
			<body>
		<?php
	}
	
	function htmlend() {
		echo "	</body>";
		echo "</html>";
	}
	
	function showIdea() {
		echo "<div id='home' class='current'>\n";
		echo "	<div class='toolbar'>\n";
		echo "		<select name='type' class='button'>\n";
		echo "			<option>1</option>\n";
		echo "			<option>2</option>\n";
		echo "		</select>";
		echo "	</div>\n";
		echo "</div>\n";
	}
	
	htmlbegin();
	
	showIdea();
	
	htmlend();

?>